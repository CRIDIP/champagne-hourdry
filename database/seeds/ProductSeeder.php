<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('products')
            ->insert([
                "name_fr"   => "Champagne Hourdry Brut",
                "name_en"   => "Champagne Hourdry Brut",
                "name_es"   => "Champagne Hourdry Brut",
                "name_de"   => "Champagne Hourdry Brut",
                "statement" => 2
            ]);

        \Illuminate\Support\Facades\DB::table('products')
            ->insert([
                "name_fr"   => "Champagne Hourdry Demi-sec",
                "name_en"   => "Champagne Hourdry Demi-sec",
                "name_es"   => "Champagne Hourdry Demi-sec",
                "name_de"   => "Champagne Hourdry Demi-sec",
                "statement" => 2
            ]);

        \Illuminate\Support\Facades\DB::table('products')
            ->insert([
                "name_fr"   => "Champagne Hourdry Millésime",
                "name_en"   => "Champagne Hourdry Millésime",
                "name_es"   => "Champagne Hourdry Millésime",
                "name_de"   => "Champagne Hourdry Millésime",
                "statement" => 2
            ]);

        \Illuminate\Support\Facades\DB::table('products')
            ->insert([
                "name_fr"   => "Champagne Hourdry Réserve",
                "name_en"   => "Champagne Hourdry Réserve",
                "name_es"   => "Champagne Hourdry Réserve",
                "name_de"   => "Champagne Hourdry Réserve",
                "statement" => 2
            ]);

        \Illuminate\Support\Facades\DB::table('products')
            ->insert([
                "name_fr"   => "Champagne Hourdry Rosé",
                "name_en"   => "Champagne Hourdry Rosé",
                "name_es"   => "Champagne Hourdry Rosé",
                "name_de"   => "Champagne Hourdry Rosé",
                "statement" => 2
            ]);
    }
}
