<?php

use Illuminate\Database\Seeder;

class UsersGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table("user_groups")->insert([
            "name_fr"   => "Administrateur",
            "name_en"   => "Administrator",
            "name_es"   => "Administrador",
            "name_de"   => "Verwalter",
        ]);

        \Illuminate\Support\Facades\DB::table("user_groups")->insert([
            "name_fr"   => "Société",
            "name_en"   => "Society",
            "name_es"   => "Empresa",
            "name_de"   => "Unternehmen",
        ]);

        \Illuminate\Support\Facades\DB::table("user_groups")->insert([
            "name_fr"   => "Comité d'entreprise",
            "name_en"   => "Works Council",
            "name_es"   => "Comite de empresa",
            "name_de"   => "Betriebsrat",
        ]);

        \Illuminate\Support\Facades\DB::table("user_groups")->insert([
            "name_fr"   => "Particulier",
            "name_en"   => "Particular",
            "name_es"   => "Particular",
            "name_de"   => "Besondere",
        ]);

        \Illuminate\Support\Facades\DB::table("user_groups")->insert([
            "name_fr"   => "CHR",
            "name_en"   => "CHR",
            "name_es"   => "CHR",
            "name_de"   => "CHR",
        ]);

        \Illuminate\Support\Facades\DB::table("user_groups")->insert([
            "name_fr"   => "Export",
            "name_en"   => "Export",
            "name_es"   => "exportación",
            "name_de"   => "Export",
        ]);
    }
}
