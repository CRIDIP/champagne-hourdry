<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            "name"      => "Administrateur",
            "email"     => "admin@champagne.com",
            "password"  => bcrypt("1992_Maxime"),
            "group_id"  => 1
        ]);
        if(env("APP_ENV") == 'local')
        {
            \Illuminate\Support\Facades\DB::table('users')->insert([
                "name"      => "SAS CRIDIP",
                "email"     => "sascridip@gmail.com",
                "password"  => bcrypt("1992_Maxime"),
                "group_id"  => 2
            ]);
            \Illuminate\Support\Facades\DB::table('users')->insert([
                "name"      => "Comité de test",
                "email"     => "cts@test.com",
                "password"  => bcrypt("1992_Maxime"),
                "group_id"  => 3
            ]);
            \Illuminate\Support\Facades\DB::table('users')->insert([
                "name"      => "John Doe",
                "email"     => "johndoe@gmail.com",
                "password"  => bcrypt("1992_Maxime"),
                "group_id"  => 4
            ]);
        }
    }
}
