<?php

use Illuminate\Database\Seeder;

class ProductPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 1,
                "group_id"          => 1,
                "conditionnement"   => 0,
                "price"             => 84.08
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 1,
                "group_id"          => 1,
                "conditionnement"   => 1,
                "price"             => 148.25
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 1,
                "group_id"          => 1,
                "conditionnement"   => 2,
                "price"             => 271.25
            ]);

        //---------------------------------------------------------------------------//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 1,
                "group_id"          => 2,
                "conditionnement"   => 0,
                "price"             => 84.08
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 1,
                "group_id"          => 2,
                "conditionnement"   => 1,
                "price"             => 148.25
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 1,
                "group_id"          => 2,
                "conditionnement"   => 2,
                "price"             => 271.25
            ]);

        //---------------------------------------------------------------------------//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 1,
                "group_id"          => 3,
                "conditionnement"   => 0,
                "price"             => 84.08
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 1,
                "group_id"          => 3,
                "conditionnement"   => 1,
                "price"             => 148.25
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 1,
                "group_id"          => 3,
                "conditionnement"   => 2,
                "price"             => 271.25
            ]);

        //===========================================================================//
        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 2,
                "group_id"          => 1,
                "conditionnement"   => 0,
                "price"             => 84.08
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 2,
                "group_id"          => 1,
                "conditionnement"   => 1,
                "price"             => 148.25
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 2,
                "group_id"          => 1,
                "conditionnement"   => 2,
                "price"             => 271.25
            ]);

        //---------------------------------------------------------------------------//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 2,
                "group_id"          => 2,
                "conditionnement"   => 0,
                "price"             => 84.08
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 2,
                "group_id"          => 2,
                "conditionnement"   => 1,
                "price"             => 148.25
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 2,
                "group_id"          => 2,
                "conditionnement"   => 2,
                "price"             => 271.25
            ]);

        //---------------------------------------------------------------------------//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 2,
                "group_id"          => 3,
                "conditionnement"   => 0,
                "price"             => 84.08
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 2,
                "group_id"          => 3,
                "conditionnement"   => 1,
                "price"             => 148.25
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 2,
                "group_id"          => 3,
                "conditionnement"   => 2,
                "price"             => 271.25
            ]);

        //===========================================================================//
        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 3,
                "group_id"          => 1,
                "conditionnement"   => 0,
                "price"             => 99.08
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 3,
                "group_id"          => 1,
                "conditionnement"   => 1,
                "price"             => 177.92
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 3,
                "group_id"          => 1,
                "conditionnement"   => 2,
                "price"             => 331.25
            ]);

        //---------------------------------------------------------------------------//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 3,
                "group_id"          => 2,
                "conditionnement"   => 0,
                "price"             => 99.08
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 3,
                "group_id"          => 2,
                "conditionnement"   => 1,
                "price"             => 177.92
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 3,
                "group_id"          => 2,
                "conditionnement"   => 2,
                "price"             => 331.25
            ]);

        //---------------------------------------------------------------------------//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 3,
                "group_id"          => 3,
                "conditionnement"   => 0,
                "price"             => 99.08
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 3,
                "group_id"          => 3,
                "conditionnement"   => 1,
                "price"             => 177.92
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 3,
                "group_id"          => 3,
                "conditionnement"   => 2,
                "price"             => 331.25
            ]);

        //===========================================================================//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 4,
                "group_id"          => 1,
                "conditionnement"   => 0,
                "price"             => 89.92
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 4,
                "group_id"          => 1,
                "conditionnement"   => 1,
                "price"             => 159.58
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 4,
                "group_id"          => 1,
                "conditionnement"   => 2,
                "price"             => 294.58
            ]);

        //---------------------------------------------------------------------------//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 4,
                "group_id"          => 2,
                "conditionnement"   => 0,
                "price"             => 89.92
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 4,
                "group_id"          => 2,
                "conditionnement"   => 1,
                "price"             => 159.58
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 4,
                "group_id"          => 2,
                "conditionnement"   => 2,
                "price"             => 294.58
            ]);

        //---------------------------------------------------------------------------//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 4,
                "group_id"          => 3,
                "conditionnement"   => 0,
                "price"             => 89.92
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 4,
                "group_id"          => 3,
                "conditionnement"   => 1,
                "price"             => 159.58
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 4,
                "group_id"          => 3,
                "conditionnement"   => 2,
                "price"             => 294.58
            ]);

        //===========================================================================//
        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 5,
                "group_id"          => 1,
                "conditionnement"   => 0,
                "price"             => 94.58
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 5,
                "group_id"          => 1,
                "conditionnement"   => 1,
                "price"             => 168.75
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 5,
                "group_id"          => 1,
                "conditionnement"   => 2,
                "price"             => 312.92
            ]);

        //---------------------------------------------------------------------------//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 5,
                "group_id"          => 2,
                "conditionnement"   => 0,
                "price"             => 94.58
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 5,
                "group_id"          => 2,
                "conditionnement"   => 1,
                "price"             => 168.75
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 5,
                "group_id"          => 2,
                "conditionnement"   => 2,
                "price"             => 312.92
            ]);

        //---------------------------------------------------------------------------//

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 5,
                "group_id"          => 3,
                "conditionnement"   => 0,
                "price"             => 94.58
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 5,
                "group_id"          => 3,
                "conditionnement"   => 1,
                "price"             => 168.75
            ]);

        \Illuminate\Support\Facades\DB::table('product_prices')
            ->insert([
                "product_id"        => 5,
                "group_id"          => 3,
                "conditionnement"   => 2,
                "price"             => 312.92
            ]);

        //===========================================================================//
    }
}
