<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_fr');
            $table->string('name_en');
            $table->string('name_es');
            $table->string('name_de');
            $table->string('text_fr')->nullable();
            $table->string('text_en')->nullable();
            $table->string('text_es')->nullable();
            $table->string('text_de')->nullable();
            $table->string('desc_fr')->nullable();
            $table->string('desc_en')->nullable();
            $table->string('desc_es')->nullable();
            $table->string('desc_de')->nullable();
            $table->integer('statement')->default(0)->comment("0: En Stock |1: Rupture |2: Délai de 7 à 15 jours");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
