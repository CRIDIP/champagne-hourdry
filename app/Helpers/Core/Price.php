<?php

if(!function_exists('getPriceForGroup')){
    function getPriceForGroup($productId, $formated = true){
        if($formated == true)
        {
            $m_user = new \App\User();
            $m_product = new \App\Model\Product\ProductPrice();
            if(auth()->guest())
            {
                return "Veuillez vous connectez";
            }else{
                $user = $m_user->newQuery()->find(auth()->user()->id)->load('group');
                $product = $m_product->newQuery()->where('product_id', $productId)->where('group_id', $user->group->id)->first();

                switch ($user->group->id)
                {
                    case 1:
                        return formatCurrency($product->price);
                    case 2:
                        return formatCurrency($product->price);
                    case 3:
                        return formatCurrency($product->price);
                    case 4:
                        return formatCurrency($product->price);
                    case 5:
                        return formatCurrency($product->price);
                    case 6:
                        return formatCurrency($product->price);
                }
            }
        }else{
            $m_user = new \App\User();
            $m_product = new \App\Model\Product\ProductPrice();
            if(auth()->guest())
            {
                return "Veuillez vous connectez";
            }else{
                $user = $m_user->newQuery()->find(auth()->user()->id)->load('group');
                $product = $m_product->newQuery()->where('product_id', $productId)->where('group_id', $user->group->id)->first();

                switch ($user->group->id)
                {
                    case 1:
                        return $product->price;
                    case 2:
                        return $product->price;
                    case 3:
                        return $product->price;
                    case 4:
                        return $product->price;
                    case 5:
                        return $product->price;
                    case 6:
                        return $product->price;
                }
            }
        }
    }
}
