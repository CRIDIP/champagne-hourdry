<?php

//if(!function_exists('createCart')){
//    function createCart(){
//
//    }
//}
use Kamaln7\Toastr\Facades\Toastr;


if(!function_exists('CartIsExist')){
    /**
     * @param $session_token
     * @return int // Retourne le nombre de commande en cours...
     */
    function CartIsExist($session_token){
        $cart = new \App\Model\Cart\Cart();
        return $cart->newQuery()->where('token', $session_token)->get()->count();
    }
}
