<?php
if(!function_exists('statement')){
    function statement($sector, $value, $styled = true){
        if($sector == 'paiementCommande'){
            if($styled == true){
                switch ($value){
                    case 0: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #F4511E; color: white"><i class="fa fa-exchange"></i> En cours d\'execution</span>';
                    case 1: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #43A047; color: white"><i class="fa fa-check"></i> Executer</span>';
                    case 2: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #E53935; color: white"><i class="fa fa-times-circle"></i> Echec du paiement</span>';
                }
            } else {
                switch ($value){
                    case 0: return 'En cours d\'execution';
                    case 1: return 'Executer';
                    case 2: return 'Echec du Paiement';
                }
            }
        }else{
            return "Etat inconnue";
        }

        if($sector == 'espaceState'){
            if($styled == true){
                switch ($value){
                    case 0: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #E53935; color: white"><i class="fa fa-power-off"></i> Hors Ligne</span>';
                    case 1: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #F4511E; color: white"><i class="fa fa-warning"></i> En Maintenance</span>';
                    case 2: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #43A047; color: white"><i class="fa fa-check-circle"></i> En Ligne</span>';
                    case 3: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #fdd835; color: black"><i class="fa fa-balance-scale"></i> Période de grace</span>';
                    case 4: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #8e24aa; color: white"><i class="fa fa-times-circle"></i> Expirer</span>';
                }
            } else {
                switch ($value){
                    case 0: return 'Hors Ligne';
                    case 1: return 'En Maintenance';
                    case 2: return 'En Ligne';
                    case 3: return 'Période de grace';
                    case 4: return 'Expirer';
                }
            }
        }else{
            return "Etat Inconnue";
        }

        if($sector == 'contratState'){
            if($styled == true){
                switch ($value){
                    case 0: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #7c7c7c; color: white"><i class="fa fa-pencil"></i> Brouillon</span>';
                    case 1: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #1e88e5; color: white"><i class="fa fa-check"></i> Valider</span>';
                    case 2: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #43a047; color: white"><i class="fa fa-signing"></i> En attente de signature</span>';
                    case 3: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #fdd835; color: black"><i class="fa fa-check-circle"></i> Executer</span>';
                    case 4: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #F4511E; color: white"><i class="fa fa-warning"></i> Bientot expirer</span>';
                    case 5: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #E53935; color: white"><i class="fa fa-times-circle"></i> Expirer</span>';
                    case 6: return '<span class="m-badge m-badge--brand m-badge--wide" style="background-color: #E53935; color: white"><i class="fa fa-times-circle"></i> Résilier</span>';
                }
            } else {
                switch ($value){
                    case 0: return 'Brouillon';
                    case 1: return 'Valider';
                    case 2: return 'En attente de signature';
                    case 3: return 'Executer';
                    case 4: return 'Bientot expirer';
                    case 5: return 'Expirer';
                    case 6: return 'Resilier';
                }
            }
        }else{
            return "Etat Inconnue";
        }

    }
}