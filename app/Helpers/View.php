<?php
if(!function_exists('formatCommande')){
    function formatCommande($value){
        if($value <= 9){
            return "CMD".date('Y')."0000".$value;
        }elseif($value > 9 && $value <= 99){
            return "CMD".date('Y')."000".$value;
        }elseif($value > 99 && $value <= 999){
            return "CMD".date('Y')."00".$value;
        }elseif($value > 999 && $value <= 9999){
            return "CMD".date('Y')."0".$value;
        }else{
            return "CMD".date('Y').$value;
        }
    }
}
if(!function_exists('formatContrat')){
    function formatContrat($value){
        if($value <= 9){
            return "CNT".date('Y')."0000".$value;
        }elseif($value > 9 && $value <= 99){
            return "CNT".date('Y')."000".$value;
        }elseif($value > 99 && $value <= 999){
            return "CNT".date('Y')."00".$value;
        }elseif($value > 999 && $value <= 9999){
            return "CNT".date('Y')."0".$value;
        }else{
            return "CNT".date('Y').$value;
        }
    }
}
if(!function_exists('formatEspace')){
    function formatEspace($value){
        if($value <= 9){
            return "ESP".date('Y')."0000".$value;
        }elseif($value > 9 && $value <= 99){
            return "ESP".date('Y')."000".$value;
        }elseif($value > 99 && $value <= 999){
            return "ESP".date('Y')."00".$value;
        }elseif($value > 999 && $value <= 9999){
            return "ESP".date('Y')."0".$value;
        }else{
            return "ESP".date('Y').$value;
        }
    }
}
if(!function_exists('formatDisabled')){
    function formatDisabled($value){
        if($value == 0){
            return "style='background-color: #929191'";
        }
    }
}
