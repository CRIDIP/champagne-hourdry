<?php
if(!function_exists('formatCurrency')){
    function formatCurrency($value, $default = false){
        if($default == false)
        {
            return number_format($value, 2, ',', ' ')." €";
        }else
        {
            return $value;
        }
    }
}

if(!function_exists('formatDate')){
    function formatDate($date, $separated = '/'){
        if($separated == '/'){
            return \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('d/m/Y');
        }else{
            return \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('d-m-Y');
        }
    }
}

if(!function_exists('formatDateTime')){
    function formatDateTime($date){
        return \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('d/m/Y à H:i');
    }
}

if(!function_exists('formatTime')){
    function formatTime($date){
        return \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('H:i:s');
    }
}

if(!function_exists('formatDateHuman')){
    function formatDateHuman($date){
        \Carbon\Carbon::setLocale('fr');
        return \Carbon\Carbon::createFromTimestamp(strtotime($date))->diffForHumans();
    }
}

if(!function_exists('formatJSON')){
    function formatJSON(array $value){
        return response()->json($value);
    }
}

if(!function_exists('formatNumCb')){
    function formatNumCb($numcb){
        return preg_replace("/[0-9]/", "X", $numcb, 12);
    }
}
