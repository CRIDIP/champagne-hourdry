<?php

namespace App\Http\Controllers\Cart;

use App\Repository\Cart\CartProductRepository;
use App\Repository\Cart\CartRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class CartController extends Controller
{
    /**
     * @var CartRepository
     */
    private $cartRepository;
    /**
     * @var CartProductRepository
     */
    private $cartProductRepository;

    /**
     * CartController constructor.
     * @param CartRepository $cartRepository
     * @param CartProductRepository $cartProductRepository
     */
    public function __construct(CartRepository $cartRepository, CartProductRepository $cartProductRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->cartProductRepository = $cartProductRepository;
    }

    public function add(Request $request)
    {
        if($this->cartRepository->cartExist() == false)
        {
            $cart = $this->cartRepository->createCart($request->get('numCustomers'));
            $product = $this->cartProductRepository->addProductCart(
                $cart->id,
                $request->get('productId'),
                $request->get('qte'),
                $request->get('price')
                );
        }else{
            $cart = $this->cartRepository->getCart($request->get('numCustomers'));

            if($this->cartProductRepository->productCartIsExists($request->get('productId'), $cart->id) == true)
            {
                $product = $this->cartProductRepository->getProductCart($cart->id, $request->get('productId'));

                $newQte = $product->qte + $request->get('qte');

                $newPrice = $product->price + $request->get('price');

                $this->cartProductRepository->updateProduct(
                    $cart->id,
                    $product->id,
                    $newQte,
                    $newPrice
                );
            }else{
                $product = $this->cartProductRepository->addProductCart(
                    $cart->id,
                    $request->get('productId'),
                    $request->get('qte'),
                    $request->get('price')
                );
            }
        }
        Toastr::success("Un Produit à été ajouter au panier !", "Panier !");
        return redirect()->back();
    }

    public function view()
    {
        $cart = $this->cartRepository->getCart(auth()->user()->numCustomers);
        $products = $this->cartProductRepository->getListProductCart($cart->id);

        //dd($cart, $products);

        return view("Cart.view", [
            "cart"  => $cart,
            "products" => $products
        ]);
    }

    public function delProduct($product_id, $cart_id)
    {
        $product = $this->cartProductRepository->delProduct($product_id, $cart_id);

        return response()->json([$product]);
    }

    public function subtotal($cart_id)
    {
        return response()->json(["subtotal" => formatCurrency(CartRepository::getGlobalAmount($cart_id))]);
    }


    public function total($cart_id)
    {
        return response()->json(["total" => formatCurrency(CartRepository::getGlobalAmount($cart_id) + 9.90)]);
    }
}
