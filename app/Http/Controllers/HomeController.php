<?php

namespace App\Http\Controllers;

use App\Repository\Product\ProductRepository;
use Illuminate\Http\Request;
use Lenius\Basket\Facades\Basket;

class HomeController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * Create a new controller instance.
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        //$this->middleware('auth');
        $this->productRepository = $productRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Basket::totalItems());
        return view('home', [
            "products"   => $this->productRepository->list()
        ]);
    }
}
