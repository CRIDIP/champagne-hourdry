<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function prices()
    {
        return $this->hasMany(ProductPrice::class);
    }
}
