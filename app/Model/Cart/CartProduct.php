<?php

namespace App\Model\Cart;

use App\Model\Product\Product;
use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
