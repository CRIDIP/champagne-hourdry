<?php

namespace App\Model\Cart;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany(CartProduct::class);
    }
}
