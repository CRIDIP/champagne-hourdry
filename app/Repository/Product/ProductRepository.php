<?php
namespace App\Repository\Product;

use App\Model\Product\Product;

class ProductRepository
{
    /**
     * @var Product
     */
    private $product;

    /**
     * ProductRepository constructor.
     * @param Product $product
     */

    public function __construct(Product $product)
    {

        $this->product = $product;
    }

    public function list()
    {
        return $this->product->newQuery()
            ->get()->load("prices");
    }

}

