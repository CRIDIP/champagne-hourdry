<?php
namespace App\Repository\Cart;

use App\Model\Cart\CartProduct;

class CartProductRepository
{
    /**
     * @var CartProduct
     */
    private $cartProduct;

    /**
     * CartProductRepository constructor.
     * @param CartProduct $cartProduct
     */

    public function __construct(CartProduct $cartProduct)
    {
        $this->cartProduct = $cartProduct;
    }

    public function addProductCart($card_id, $product_id, $qte, $price)
    {
        return $this->cartProduct->newQuery()
            ->create([
                "card_id"       => $card_id,
                "product_id"    => $product_id,
                "qte"           => $qte,
                "price"         => $price
            ]);
    }

    public function productCartIsExists($product_id, $cart_id)
    {
        $count = $this->cartProduct->newQuery()
            ->where('card_id', $cart_id)
            ->where('product_id', $product_id)
            ->get()
            ->count();

        if($count == 0)
        {
            return false;
        }else{
            return true;
        }
    }

    public function getProductCart($cart_id, $product_id)
    {
        return $this->cartProduct->newQuery()
            ->where('card_id', $cart_id)
            ->where('product_id', $product_id)
            ->get()
            ->first();
    }

    public function updateProduct($card_id, $product_id, $qte, $price)
    {
        return $this->cartProduct->newQuery()
            ->where('card_id', $card_id)
            ->where('product_id', $product_id)
            ->update([
                "qte"   => $qte,
                "price" => $price
            ]);
    }

    public function getListProductCart($card_id)
    {
        return $this->cartProduct->newQuery()
            ->where("card_id", $card_id)
            ->get()
            ->load('product');
    }

    public function delProduct($product_id, $cart_id)
    {
        return $this->cartProduct->newQuery()
            ->where('card_id', $cart_id)
            ->where('id', $product_id)
            ->delete();
    }

}

