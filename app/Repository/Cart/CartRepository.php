<?php
namespace App\Repository\Cart;

use App\Model\Cart\Cart;
use App\Model\Cart\CartProduct;

class CartRepository
{
    /**
     * @var Cart
     */
    private $cart;

    /**
     * CartRepository constructor.
     * @param Cart $cart
     */

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function cartExist()
    {
        $count = $this->cart->newQuery()->where('token', session()->get('_token'))->get()->count();
        if($count == 0)
        {
            return false;
        }else{
            return true;
        }
    }

    public function createCart($numCustomers)
    {
        return $this->cart->newQuery()
            ->create([
                "numCustomers"  => $numCustomers,
                "token"         => session()->get('_token')
            ]);
    }

    public function getCart($numCustomers)
    {
        return $this->cart->newQuery()
            ->where('numCustomers', $numCustomers)
            ->where('token', session()->get('_token'))
            ->get()
            ->first();
    }

    public static function getGlobalAmount($cart_id)
    {
        $cart = new CartProduct();

        $global = $cart->newQuery()->where('card_id', $cart_id)->sum('price');

        return $global;

    }

}

