<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateView extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'view:create {dossier} {vue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Créer une vue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(empty($this->argument('dossier'))){
            $namespace = "namespace App\Repository;";
        }else{
            $namespace = "namespace App\Repository\\".$this->argument('dossier').";";
        }

        $vue = $this->argument('vue');
        $dossierOutput = "resources/views/".$this->argument('dossier');

        $content = "@extends('Layout.app')

@section(\"styles\")
    
@endsection

@section('subHeader')
    <div class='m-subheader'>
        <div class='d-flex align-items-center'>
            <div class='mr-auto'>
                <h3 class='m-subheader__title'>
                    Dashboard
                </h3>
            </div>
            <!--<div>
                <a href=\"\" class=\"btn btn-primary m-btn m-btn--icon m-btn--pill\"><i class=\"fa fa-arrow-circle-left\"></i> Retour</a>
            </div>-->
        </div>
    </div>
@endsection

@section('content')

@endsection

@section('scripts')

@endsection
        ";
        $this->isDir($dossierOutput);
        $stat = file_put_contents($dossierOutput."/".$vue.".blade.php", $content);
        if($stat == false){
            $this->error('Impossible de créer le fichier');
        }else{
            $this->info("Fichier de vue créer");
        }
    }

    private function isDir($dossier){
        if(!is_dir($dossier)){
            mkdir($dossier);
        }
    }
}
