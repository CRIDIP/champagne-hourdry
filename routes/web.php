<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');
Route::group(["middleware" => ["auth"], "prefix" => "account", "namespace" => "Account"], function (){
    Route::get('/', ["as" => "Account.index", "uses" => "AccountController@index"]);
    Route::get('/whitelist', ["as" => "Account.whitelist", "uses" => "AccountController@whitelist"]);
    Route::get('/checkout', ["as" => "Account.checkout", "uses" => "AccountController@checkout"]);
});

Route::group(["prefix" => "cart", "namespace" => "Cart"], function (){
    Route::get('/', ["as" => "Cart.view", "uses" => "CartController@view"]);
    Route::post('add', ["as" => "Cart.add", "uses" => "CartController@add"]);

    Route::delete('{product_id}/{cart_id}', ["as" => "Cart.delProduct", "uses" => "CartController@delProduct"]);
    Route::get('refreshSubtotal/{cart_id}', ["as" => "Cart.subtotal", "uses" => "CartController@subtotal"]);
    Route::get('refreshTotal/{cart_id}', ["as" => "Cart.subtotal", "uses" => "CartController@total"]);


});

Route::group(["prefix" => "product", "namespace" => "Product"], function (){
    Route::get('/', ["as" => "Product.index", "uses" => "ProductController@index"]);
    Route::get('/{id}', ["as" => "Product.show", "uses" => "ProductController@show"]);
});

Auth::routes();
Route::get('logout', "Auth\LoginController@logout");
Route::get('/code', "TestController@code");



