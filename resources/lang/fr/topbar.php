<?php

return [

    /*
    |--------------------------------------------------------------------------
    | TopBar Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    "account"   => "Mon Compte",
    "whitelist" => "Liste Blanche",
    "checkout"  => "Mes Commandes",
    "login"     => "Connexion",
    "logout"    => "Déconnexion",
    "l_current" => "Français",
    "l_fr"      => "Français",
    "l_en"      => "Anglais",
    "l_es"      => "Espagnol",
    "l_de"      => "Allemand"

];
