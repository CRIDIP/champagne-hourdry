<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Header Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    "error" => [
        "error" => "Erreur !",
        "addProductCart"  => "Erreur lors de l\'ajout du produit dans le panier !",
        "delProductCart"  => "Erreur lors de la suppression du produit dans le panier !",
        "editQteProductCart"  => "Erreur lors de la modification du produit dans le panier !",
    ],
    "success" => [
        "success"   => "Succes",
        "addProductCart"    => "Le produit a ete ajouter au panier",
        "delProductCart"    => "Le produit a ete supprimer du panier",
        "editQteProductCart"=> "Le produit a ete modifier dans le panier"
    ]

];
