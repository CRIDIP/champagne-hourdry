<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'continue_shop' => "Continuer mes achats",
    'checkout'      => "Commander",
    'pending_order' => "Commande en cours",
    'image'         => "Image",
    'description'   => "Description",
    'reference'     => "Ref No",
    'quantity'      => "Quantité",
    'unit_price'    => "Prix Unitaire",
    'total'         => "Total",
    'subtotal'      => "Sous-Total",
    'shipping_cost' => "Frais de Livraison"

];
