<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Header Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    "items"             => "{0} objet|[2,*] objets",
    "emptyCart"         => "Votre panier est vide",
    "viewCart"          => "Voir le Panier",
    "checkout"          => "Commander",
    "m_history"         => "Histoire",
    "m_gamme"           => "Gammes",
    "m_perso"           => "Personnalisation",
    "m_contact"         => "Contact"

];
