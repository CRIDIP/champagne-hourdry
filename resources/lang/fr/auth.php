<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ces informations d\'identification ne correspondent pas à nos enregistrements.',
    'throttle' => 'Trop de tentatives de connexion. Veuillez réessayer dans :seconds secondes.',
    'new_customers' => "Nouveau Client",
    'new_customers_binding' => "En créant un compte, vous pourrez faire vos achats plus rapidement, être au courant du statut d’une commande et suivre les commandes que vous avez précédemment passées.",
    'ret_customers' => "Déja Client",
    'email' => "Adresse Mail",
    'password' => "Mot de passe",
    'forgot_pass' => "Mot de passe oublié ?",
    'login'    => "Connexion",
    ''

];
