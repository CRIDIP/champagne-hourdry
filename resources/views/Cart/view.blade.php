@extends('layouts.app')

@section("css")
    <!-- Page level plugin styles START -->
    <link href="/assets/pages/css/animate.css" rel="stylesheet">
    <link href="/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
    <link href="/assets/plugins/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
    <!-- Page level plugin styles END -->
@endsection

@section('content')
    <div class="main">
        <div class="container">
            <!-- BEGIN SIDEBAR & CONTENT -->
            <div class="row margin-bottom-40">
                <!-- BEGIN CONTENT -->
                <div class="col-md-12 col-sm-12">
                    <h1>{{ trans("cart.pending_order") }}</h1>
                    <div class="goods-page">
                        <div class="goods-data clearfix">
                            <div class="table-wrapper-responsive">
                                <table summary="Shopping cart" id="cart">
                                    <tr>
                                        <th class="goods-page-image">{{ trans("cart.image") }}</th>
                                        <th class="goods-page-description">{{ trans("cart.description") }}</th>
                                        <th class="goods-page-ref-no">{{ trans("cart.reference") }}</th>
                                        <th class="goods-page-quantity">{{ trans("cart.quantity") }}</th>
                                        <th class="goods-page-price">{{ trans("cart.unit_price") }}</th>
                                        <th class="goods-page-total" colspan="2">{{ trans("cart.total") }}</th>
                                    </tr>
                                    @foreach($products as $product)
                                    <tr>
                                        <td class="goods-page-image">
                                            <a href="javascript:;"><img src="/storage/img/product/{{ $product->product_id }}-{{ $product->product_id }}.jpg" alt="{{ $product->product->name_fr }}"></a>
                                        </td>
                                        <td class="goods-page-description">
                                            <h3><a href="javascript:;">{{ $product->product->name_fr }}</a></h3>
                                            <em>{{ substr($product->product->text_fr, 0, 255) }}</em>
                                        </td>
                                        <td class="goods-page-ref-no">
                                            {{ $product->product_id }}
                                        </td>
                                        <td class="goods-page-quantity">
                                            <div class="product-quantity">
                                                <input id="product-quantity" type="text" value="{{ $product->qte }}" readonly class="form-control input-sm">
                                            </div>
                                        </td>
                                        <td class="goods-page-price">
                                            <strong>{{ getPriceForGroup($product->product_id, false) }} <span>€</span></strong>
                                        </td>
                                        <td class="goods-page-total">
                                            <strong>{{ getPriceForGroup($product->product_id, false) * $product->qte }} <span>€</span></strong>
                                        </td>
                                        <td class="del-goods-col">
                                            <a class="del-goods" id="del-product" href="{{ route("Cart.delProduct", [$product->id, $cart->id]) }}">&nbsp;</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>

                            <div class="shopping-total">
                                <ul>
                                    <li>
                                        <em>{{ trans("cart.subtotal") }}</em>
                                        <strong class="price" id="subtotal" data-id="{{ $cart->id }}">{{ formatCurrency(\App\Repository\Cart\CartRepository::getGlobalAmount($cart->id)) }}</strong>
                                    </li>
                                    <li>
                                        <em>{{ trans("cart.shipping_cost") }}</em>
                                        <strong class="price" id="shippingcoast">9,90 <span>€</span></strong>
                                    </li>
                                    <li class="shopping-total-price">
                                        <em>{{ trans("cart.total") }}</em>
                                        <strong class="price" id="total" data-id="{{ $cart->id }}">{{ formatCurrency(\App\Repository\Cart\CartRepository::getGlobalAmount($cart->id) + 9.90) }}</strong>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <a class="btn btn-default" href="{{ route('home') }}">{{ trans("cart.continue_shop") }} <i class="fa fa-shopping-cart"></i></a>
                        <button class="btn btn-primary" type="submit">{{ trans("cart.checkout") }} <i class="fa fa-check"></i></button>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END SIDEBAR & CONTENT -->

            <!-- BEGIN SIMILAR PRODUCTS -->

            <!-- END SIMILAR PRODUCTS -->
        </div>
    </div>
@endsection

@section("script")
    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <script src="/assets/plugins/owl.carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
    <script src='/assets/plugins/zoom/jquery.zoom.min.js' type="text/javascript"></script><!-- product zoom -->
    <script src="/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->

    <script src="/assets/corporate/scripts/layout.js" type="text/javascript"></script>
    <script src="/assets/pages/scripts/bs-carousel.js" type="text/javascript"></script>
    <script type="text/javascript">
        function refreshSubtotal()
        {
            let div = $("#subtotal");
            let cartId = div.attr('data-id');
            let url = '/cart/refreshSubtotal/'+cartId

            $.ajax({
                url: url,
                success: function (data) {
                    div.html(data.subtotal)
                },
                error: function () {
                    toastr.error("Impossible de mettre à jour le sous total du panier", "Panier");
                }
            })
        }
        function refreshTotal()
        {
            let div = $("#subtotal");
            let cartId = div.attr('data-id');
            let url = '/cart/refreshTotal/'+cartId

            $.ajax({
                url: url,
                success: function (data) {
                    div.html(data.total)
                },
                error: function () {
                    toastr.error("Impossible de mettre à jour le total du panier", "Panier");
                }
            })
        }
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
            Layout.initOWL();
            Layout.initImageZoom();
            Layout.initTouchspin();
            Layout.initTwitter();

            Layout.initFixHeaderWithPreHeader();
            Layout.initNavScrolling();

            $("#cart").on('click', '#del-product', function (e) {
                e.preventDefault()
                let btn = $(this)
                let url = btn.attr('href')

                $.ajax({
                    url: url,
                    type: "DELETE",
                    success: function (data) {
                        btn.parents('tr').fadeOut()
                        toastr.success("Le produit à été supprimer du panier !", "Panier")
                        refreshSubtotal()
                        refreshTotal()
                    },
                    error: function (jqxhr, data) {
                        toastr.error(jqxhr.responseText)
                    },
                    always: function () {
                        btn.text('&nbsp;')
                    }
                })
            })

        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
@endsection
