@extends('layouts.app')

@section("css")
    <!-- Page level plugin styles START -->
    <link href="/assets/pages/css/animate.css" rel="stylesheet">
    <link href="/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
    <link href="/assets/plugins/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
    <!-- Page level plugin styles END -->
@endsection

@section('content')
    <!-- BEGIN SLIDER -->
    <div class="page-slider margin-bottom-35">
        <div id="carousel-example-generic" class="carousel slide carousel-slider">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <!-- First slide -->
                <div class="item carousel-item-four active">
                    <div class="container">
                        <div class="carousel-position-four text-center">
                            <h2 class="margin-bottom-20 animate-delay carousel-title-v3 border-bottom-title text-uppercase" data-animation="animated fadeInDown">
                                Tones of <br/><span class="color-red-v2">Shop UI Features</span><br/> designed
                            </h2>
                            <p class="carousel-subtitle-v2" data-animation="animated fadeInUp">Lorem ipsum dolor sit amet constectetuer diam <br/>
                                adipiscing elit euismod ut laoreet dolore.</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control carousel-control-shop" href="#carousel-example-generic" role="button" data-slide="prev">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </a>
            <a class="right carousel-control carousel-control-shop" href="#carousel-example-generic" role="button" data-slide="next">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    <!-- END SLIDER -->

    <div class="main">
        <div class="container">
            <!-- BEGIN SALE PRODUCT & NEW ARRIVALS -->
            <div class="row margin-bottom-40">
                <!-- BEGIN SALE PRODUCT -->
                <div class="col-md-12 sale-product">
                    <h2>{{ trans("home.products") }}</h2>
                    <div class="owl-carousel owl-carousel5">
                        @foreach($products as $product)
                        <div>
                            <div class="product-item">
                                <div class="pi-img-wrapper">
                                    <img src="/storage/img/product/{{ $product->id }}.jpg" class="img-responsive" alt="Berry Lace Dress">
                                    <div>
                                        <a href="/storage/img/product/{{ $product->id }}.jpg" class="btn btn-default fancybox-button">{{ trans("home.zoom") }}</a>
                                        <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">{{ trans("home.view") }}</a>
                                    </div>
                                </div>
                                <h3><a href="shop-item.html">{{ $product->name_fr }}</a></h3>
                                <div class="pi-price">{{ getPriceForGroup($product->id) }}</div>
                                @auth()
                                    {{ Form::open(["route" => "Cart.add", "id" => "addCart"]) }}
                                    {{ Form::hidden("numCustomers", auth()->user()->numCustomers) }}
                                    {{ Form::hidden("productId", $product->id) }}
                                    {{ Form::hidden("libelle", $product->name_fr) }}
                                    {{ Form::hidden("qte", 1) }}
                                    {{ Form::hidden("price", getPriceForGroup($product->id, false)) }}
                                    <button id="add2cart" class="btn btn-default add2cart">{{ trans('home.add_cart') }}</button>
                                    {{ Form::close() }}
                                @endauth
                                <!--<div class="sticker sticker-sale"></div>-->
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
                <!-- END SALE PRODUCT -->
            </div>
            <!-- END SALE PRODUCT & NEW ARRIVALS -->
        </div>
    </div>


    <!-- BEGIN STEPS -->
    <div class="steps-block steps-block-red">
        <div class="container">
            <div class="row">
                <div class="col-md-4 steps-block-col">
                    <i class="fa fa-truck"></i>
                    <div>
                        <h2>Free shipping</h2>
                        <em>Express delivery withing 3 days</em>
                    </div>
                    <span>&nbsp;</span>
                </div>
                <div class="col-md-4 steps-block-col">
                    <i class="fa fa-gift"></i>
                    <div>
                        <h2>Daily Gifts</h2>
                        <em>3 Gifts daily for lucky customers</em>
                    </div>
                    <span>&nbsp;</span>
                </div>
                <div class="col-md-4 steps-block-col">
                    <i class="fa fa-phone"></i>
                    <div>
                        <h2>477 505 8877</h2>
                        <em>24/7 customer care available</em>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END STEPS -->
@endsection

@section("script")
    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <script src="/assets/plugins/owl.carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
    <script src='/assets/plugins/zoom/jquery.zoom.min.js' type="text/javascript"></script><!-- product zoom -->
    <script src="/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->

    <script src="/assets/corporate/scripts/layout.js" type="text/javascript"></script>
    <script src="/assets/pages/scripts/bs-carousel.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
            Layout.initOWL();
            Layout.initImageZoom();
            Layout.initTouchspin();
            Layout.initTwitter();

            Layout.initFixHeaderWithPreHeader();
            Layout.initNavScrolling();

        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
@endsection
