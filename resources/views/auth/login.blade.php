@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel-body row">
        <div class="col-md-6 col-sm-6">
            <h3>{{ trans('auth.new_customers') }}</h3>
            <p>{{ trans('auth.new_customers_binding') }}</p>
            <button class="btn btn-primary" type="submit" data-toggle="collapse" data-parent="#checkout-page" data-target="#payment-address-content">Continue</button>
        </div>
        <div class="col-md-6 col-sm-6">
            <h3>{{ trans('auth.ret_customers') }}</h3>
            {{ Form::open(["route" => "login"]) }}
            <div class="form-group">
                {{ Form::label("email", trans("auth.email")) }}
                {{ Form::email("email", null, ["class" => "form-control"]) }}
            </div>
            <div class="form-group">
                {{ Form::label("password", trans("auth.password")) }}
                {{ Form::password("password", ["class" => "form-control"]) }}
            </div>
            <a href="javascript:;">{{ trans("auth.forgot_pass") }}</a>
            <div class="padding-top-20">
                <button class="btn btn-primary" type="submit">{{ trans('auth.login') }}</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
