<!-- BEGIN TOP BAR -->
<div class="pre-header">
    <div class="container">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->
            <div class="col-md-6 col-sm-6 additional-shop-info">
                <ul class="list-unstyled list-inline">
                    <li><i class="fa fa-phone"></i><span>0892 492 648</span></li>
                    <!-- BEGIN CURRENCIES -->
                    <li class="shop-currencies">
                        <a href="javascript:void(0);" class="current">€</a>
                        <a href="javascript:void(0);">£</a>
                        <a href="javascript:void(0);">$</a>
                    </li>
                    <!-- END CURRENCIES -->
                    <!-- BEGIN LANGS -->
                    <li class="langs-block">
                        <a href="javascript:void(0);" class="current">{{ __("topbar.l_current") }} </a>
                        <div class="langs-block-others-wrapper"><div class="langs-block-others">
                                <a href="javascript:void(0);">{{ __("topbar.l_en") }}</a>
                                <a href="javascript:void(0);">{{ __("topbar.l_de") }}</a>
                                <a href="javascript:void(0);">{{ __("topbar.l_es") }}</a>
                                <a href="javascript:void(0);">{{ __("topbar.l_fr") }}</a>
                            </div></div>
                    </li>
                    <!-- END LANGS -->
                </ul>
            </div>
            <!-- END TOP BAR LEFT PART -->
            <!-- BEGIN TOP BAR MENU -->
            <div class="col-md-6 col-sm-6 additional-nav">
                <ul class="list-unstyled list-inline pull-right">
                    @auth()
                        <li><a href="{{ route("Account.index") }}">{{ trans("topbar.account") }}</a></li>
                        <li><a href="{{ route("Account.whitelist") }}">{{ trans("topbar.whitelist") }}</a></li>
                        <li><a href="{{ route('Account.checkout') }}">{{ trans("topbar.checkout") }}</a></li>
                        <li><a id="btnLogout" href="{{ route('logout') }}">{{ trans("topbar.logout") }}</a></li>
                    @else
                        <li><a href="{{ route('login') }}">{{ trans("topbar.login") }}</a></li>
                    @endauth
                </ul>
            </div>
            <!-- END TOP BAR MENU -->
        </div>
    </div>
</div>
<!-- END TOP BAR -->
