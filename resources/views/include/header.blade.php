<!-- BEGIN HEADER -->
<div class="header">
    <div class="container">
        <a class="site-logo" href="{{ route('home') }}"><img src="/assets/corporate/img/logos/logo-shop-red.png" alt="{{ env("APP_NAME") }}"></a>

        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        @if(CartIsExist(session()->get('_token')) != 0)
            <div class="top-cart-block">
                <div class="top-cart-info">
                    <a href="{{ route('Cart.view') }}" class="top-cart-info-count">Vous avez une commande en cours</a>
                </div>
                <i class="fa fa-shopping-cart"></i>

            </div>
        @endif

        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation">
            <ul>
                <li><a href="shop-item.html">{{ trans("header.m_history") }}</a></li>
                <li><a href="shop-item.html">{{ trans("header.m_gamme") }}</a></li>
                <li><a href="shop-item.html">{{ trans("header.m_perso") }}</a></li>
                <li><a href="shop-item.html">{{ trans("header.m_contact") }}</a></li>

                <!-- BEGIN TOP SEARCH -->
                <!--<li class="menu-search">
                    <span class="sep"></span>
                    <i class="fa fa-search search-btn"></i>
                    <div class="search-box">
                        <form action="#">
                            <div class="input-group">
                                <input type="text" placeholder="Search" class="form-control">
                                <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit">Search</button>
                    </span>
                            </div>
                        </form>
                    </div>
                </li>-->
                <!-- END TOP SEARCH -->
            </ul>
        </div>
        <!-- END NAVIGATION -->
    </div>
</div>
<!-- Header END -->
